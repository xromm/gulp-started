require ‘busy’
require ‘breakpoint’

project_type = :stand_alone
http_path = “/“
sass_path = “sass”
css_dir = “css”
images_dir = “images”
fonts_dir = “fonts”
javascript_dir = “js”
line_comments = false
preferred_syntax = :sass
relative_assets = true